namespace go client

struct Mail {
    1: string remoteEmail,
    2: string body
}

struct Client {
    1: i32 id,
    2: string name,
    3: string email,
    4: string country,
    5: Mail inbox
}