package test

import (
	"bytes"
	"fmt"
	"net/http"
	"testing"

	"proto/client_example/pkg/protos/client"

	"github.com/golang/protobuf/proto"
)

func TestClient(t *testing.T) {
	myClient := client.Client{Id: 526, Name: "Luis Colmenarez", Email: "luis.colmenarez@innova4j.com", Country: "VE"}
	clientInbox := make([]*client.Client_Mail, 0, 20)
	clientInbox = append(clientInbox,
		&client.Client_Mail{RemoteEmail: "luis.colmenarez@innova4j.com", Body: "Hola, esto es un test."},
		&client.Client_Mail{RemoteEmail: "luis.miguelcv2@gmail.com", Body: "Test, Chao"})

	myClient.Inbox = clientInbox

	data, err := proto.Marshal(&myClient)
	if err != nil {
		fmt.Println(err)
		return
	}

	_, err = http.Post("http://localhost:9090", "", bytes.NewBuffer(data))

	if err != nil {
		fmt.Println(err)
		return
	}
}
