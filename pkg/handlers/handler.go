package handlers

import (
	"log"
	"net/http"
	"time"

	"proto/client_example/pkg/controllers"

	"github.com/gorilla/mux"
)

func SetUp() {
	router := mux.NewRouter()
	router.HandleFunc("/", controllers.PrintContent).Methods("POST")

	srv := &http.Server{
		Handler:      router,
		Addr:         "127.0.0.1:9090",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
