package controllers

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"proto/client_example/pkg/protos/client"

	"github.com/golang/protobuf/proto"
)

func PrintContent(w http.ResponseWriter, r *http.Request) {
	myClient := client.Client{}

	data, err := ioutil.ReadAll(r.Body)

	if err != nil {
		fmt.Println(err)
	}

	if err := proto.Unmarshal(data, &myClient); err != nil {
		fmt.Println(err)
	}

	println(myClient.Id, ":", myClient.Name, ":", myClient.Email, ":", myClient.Country)

	for _, mail := range myClient.Inbox {
		fmt.Println(mail.RemoteEmail, ":", mail.Body)
	}
}
